﻿Shader "Customized/WaterScroller" 
{
    Properties 
    {
		_MainTex ("Water", 2D) = "white" {}
		_FoamTex ("Foam", 2D) = "white" {}
		_Color ("Color (RGBA)", Color) = (1, 1, 1, 1) 
		_ScrollX ("Scroll X", Range(-5,5)) = 1
		_ScrollY ("Scroll Y", Range(-5,5)) = 1
		_Visble ("Visble", Range(0,1)) = 1
    }

    SubShader 
    {
        Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
        Cull front 
        LOD 100

        Pass 
        {
            CGPROGRAM

            #pragma vertex vert alpha
            #pragma fragment frag alpha

            #include "UnityCG.cginc"

            struct appdata_t 
            {
                float4 vertex   : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f 
            {
                float4 vertex  : SV_POSITION;
                half2 texcoord : TEXCOORD0;
            };
			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _FoamTex;
			float4 _Color;
			float _ScrollX;
			float _ScrollY;
			float _Visble;

            v2f vert (appdata_t v)
            {
                v2f o;

                o.vertex     = UnityObjectToClipPos(v.vertex);
                v.texcoord.x = 1 - v.texcoord.x;
                o.texcoord   = TRANSFORM_TEX(v.texcoord, _MainTex);

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				_ScrollX *= _Time;
				_ScrollY *= _Time;
				float3 water = (tex2D (_MainTex, i.texcoord + float2(_ScrollX, _ScrollY))).rgb;
				float3 foam = (tex2D (_FoamTex, i.texcoord + float2(_ScrollX/2.0, _ScrollY/2.0))).rgb;
                fixed4 col;
				col.rgb = (water + foam)/2.0 * _Color; // multiply by _Color
                col.w = _Visble;     
                return col;
            }

            ENDCG
        }
    }
}
