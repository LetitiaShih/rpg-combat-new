﻿using UnityEngine;
using RPG.Combat;
using RPG.Saving;

namespace RPG.Core
{
    public class Health : MonoBehaviour, ISaveable
    {
        [SerializeField] float healthPoint = 100f;
        bool isDead = false;
        public bool IsDead(){
            return isDead;
        }
        public void TakeDamage(float damage)
        {
            healthPoint = Mathf.Max(healthPoint - damage, 0);
            print(healthPoint);
            if(healthPoint == 0)
            {
                Die();

            }
        }

        private void Die()
        {
            if(isDead){return;}
            isDead = true;
            Animator animator = GetComponent<Animator>();
            animator.SetTrigger("die");
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }
        public object CaptureState()
        {
            return healthPoint;
        }

        public void RestoreState(object state)
        {
            healthPoint = (float) state;

            if (healthPoint <= 0)
            {
                Die();
            }
        }
    }
} 
