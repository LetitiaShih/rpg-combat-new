﻿namespace RPG.Saving
{
    public interface ISaveable
    {
        object CaptureState();
        void RestoreState(object state);
    }
} 
/*
for decreasing the dependencies on SaveableEntity, we create a interface
and the attributes implement this interface. so SaveableEntity can call Isaveable 
for capture, restore data :) 
 */