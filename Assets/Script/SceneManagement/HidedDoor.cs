﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidedDoor : MonoBehaviour
{
    [SerializeField]
    [Range(0, 1)]
    float speed = 1;
    float _alpha = 1;
    bool _doorKnocked = false;
    bool _inVisibleTime, _visibleTime = false;
    public GameObject InvisibleObj;
    public GameObject VisibleObj;
    Material _invisibleMaterial, _visibleMaterial;
    // Start is called before the first frame update
    void Start()
    {
        // _material = gameObject.GetComponent<MeshRenderer>().material;
        _invisibleMaterial = InvisibleObj.GetComponent<MeshRenderer>().material;
        _visibleMaterial = VisibleObj.GetComponent<MeshRenderer>().material;
        _inVisibleTime = true; //knock gate
    }

    // Update is called once per frame
    void Update()
    {   
        //if(_doorKnocked )
        if(_inVisibleTime)
        {
            TweenInvisibleDoor();
        }
        else if(_visibleTime)
        {
            TweenVisibleDoor();
        }
    }
    void OnTriggerEnter(Collider other) 
    {
        if(other.tag == "Player") //or weapon
        {
            _doorKnocked = true;
        }
    }
    void TweenInvisibleDoor()
    {
        if(_alpha >= 0)
        {
            _alpha -= speed * Time.deltaTime; //Speed = 1 -> 1sec
            _invisibleMaterial.SetFloat("_Visble", _alpha);
        }
        else
        {
            _inVisibleTime = false;
            _visibleTime = true;
        }
    }
    void TweenVisibleDoor()
    {
        if(_alpha <= 1)
        {
            _alpha += speed * Time.deltaTime; //Speed = 1 -> 1sec
            _visibleMaterial.SetFloat("_Visble", _alpha);    
        }
        else
        {
            _visibleTime = false;
        }
    }
}
