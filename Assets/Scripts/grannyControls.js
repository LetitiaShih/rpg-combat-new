﻿#pragma strict

static var anim: Animator;
private var speed: float = 0.2f;
var runSound: AudioSource;
var puffSound: AudioSource;
var body: GameObject;
var blueMaterial: Material;
var redMaterial: Material;
var skyMaterial: Material;
var superPowers: boolean;
var theSun: GameObject;

function Start () {

	anim = gameObject.GetComponent(Animator);
	superPowers = false;

}

function Update () 
{
	//keyboard controls
	if(Input.GetKey("space"))
	{
	  anim.SetTrigger("jump");
	}
	else if(Input.GetKey("left"))
	{
	  this.transform.Rotate(Vector3.up, -1);
	}
	else if(Input.GetKey("right"))
	{
	  this.transform.Rotate(Vector3.up, 1);
	}
	
		
	if(Input.GetKey("up"))
	{
	  
	  if(runSound && !runSound.isPlaying)  runSound.Play();
	  if(puffSound && !puffSound.isPlaying)  puffSound.Play();
	  anim.SetBool("running",true);
	  this.transform.position += this.transform.forward * speed;
	}
	else if(Input.GetKey("down"))
	{
	  if(runSound && !runSound.isPlaying)  runSound.Play();
	  if(puffSound && !puffSound.isPlaying)  puffSound.Play();
	  anim.SetBool("running",true);
	  this.transform.position -= this.transform.forward * speed;
	}
	else if(Input.GetKeyUp("up") || Input.GetKeyUp("down"))
	{
		if(runSound) runSound.Stop();
		if(puffSound) puffSound.Stop();
		anim.SetBool("running",false);
	}


	if(Input.GetKeyDown("p")) //turn on superpowers
	{
		superPowers = !superPowers;
		if(superPowers)
		{
			body.GetComponent(Renderer).material = redMaterial;
		}
		else
			body.GetComponent(Renderer).material = blueMaterial;
		gameObject.GetComponent(Rigidbody).isKinematic = !gameObject.GetComponent(Rigidbody).isKinematic;
		gameObject.GetComponent(Collider).isTrigger = !gameObject.GetComponent(Collider).isTrigger;
	}

	if(Input.GetKeyDown("1"))
	{
		RenderSettings.fog = !RenderSettings.fog;
	}

	if(Input.GetKeyDown("2"))
	{
		if(RenderSettings.skybox == skyMaterial)
		{
			RenderSettings.skybox = null;
		}
		else
			RenderSettings.skybox = skyMaterial; 
	}

	if(Input.GetKeyDown("3"))
	{
		theSun.SetActive(!theSun.activeSelf);
	}

	
}