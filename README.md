# Reset everything:
$ git reset --hard

# Pull from remote (Download remote master)
$ git pull

# Sync from remote branch
$ git pull --force

